﻿# Trabajo practico 2

## Uso de Markdown, resumen



Markdown es un lenguaje de marcado que facilita el proceso de darle formato a un texto mediante símbolos entre las palabras. Está diseñado para ser ligero y ágil reemplazando los pesados y complejos editores de texto. 

Esta es una lista a modo de resumen de las características que puede añadirse al texto en Markdown, junto con los símbolos necesarios, con el fin de agregar formato al texto:



- Resaltar textos a modo de títulos:

("#" antes del texto) Texto de primer tamaño

("##" antes del texto) Texto de segundo tamaño

("###" antes del texto) Texto de tercer tamaño



- Agregar formato de tabla:

("-" antes del texto)



- Formula matemática:

("$$" antes y después de la expresión de la fórmula)



- Insertar imagen a través de link:



("![]" antes del link de la imagen)



- Negrita:

("**" antes y después del texto)



- Cursiva:

("_" antes y después del texto)





- Diagrama simple:



```flow

st=>start: Entrada

op=>operation: Operacion realizada

cond=>condition: Condicion si o no 

e=>end: salida



st->op->cond

cond(yes)->e

cond(no)->op

```



- Insertar link:

("[palabra sobre la que se aplica]"(link que se desea insertar))



Hay más comandos útiles pero su uso está destinado, más que nada, a la creación y mantenimiento de páginas web, por lo que no los destacaremos.