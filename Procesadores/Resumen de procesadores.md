﻿# Trabajo practico 2. Organización de una computadora


### Objetivos

- Describir las partes que componen una computadora y desarrollar como es que estas partes trabajan en conjunto.



### Alumno - Carrera

- Gastón Villafañe - Ingeniería Mecánica.



### Docentes responsables

- René Cejas Bolecek.

- Martin Vilugrón.







## Procesadores

Una computadora consiste en un conjunto de procesadores trabajando interconectados, memorias y dispositivos de entrada y de salida. Analizaremos cada uno por separado y veremos como se relacionan entre si. 

El cerebro de una computadora se llama CPU (Unidad central de procesamiento) y es el mencionado procesador de la computadora. Allí es donde se llevan a cabo todas las operaciones y el seguimiento de instrucciones que pueden realizarse. Esta, se compone de tres partes, la unidad de control, la unidad de aritmética y lógica (ALU) y los registros.

La unidad de control es la encarga de buscar los programas en la memoria y determinar su tipo, mientras que la unidad ALU se encarga de realizar las operaciones lógicas (como por ejemplo "suma") necesarias para realizar las instrucciones. Por último los registros son partes de la memoria que tiene integrado el procesador, estos se encargan de almacenar información temporal importante, como las instrucciones del programa que sé está ejecutando (Registro de instrucciones) o la siguiente instrucción que de buscarse y ejecutar (Contador de programa).

El llamado cicló de datos es el proceso que se lleva a cabo para realizar las instrucciones de los programas. En este la unidad de control busca las instrucciones en la memoria y las mueve a los registros, como ejemplo estas instrucciones pueden ser "sumar dos numero enteros", luego ambos numero almacenados en los registros pasan a la ALU donde son operados y sumados, luego el resultado es devuelto al registro nuevamente donde es almacenado temporalmente y de donde puede ser llevado a la memoria para almacenar definitivamente el resultado. Estos pasos que realiza la ALU se llama camino de datos y el echo de devolver el resultado al registro completa un circuito y se llama ciclo de datos.

### Pasos que realiza el procesador

El CPU ejecuta las instrucciones en la siguiente serie de pasos:

- Busca la siguiente instrucción en la memoria y la coloca en el registro

- Modifica el contador de programa de modo que apunte a la siguiente instrucción

- Determina que tipo de instrucción es

- Si la instrucción utilizada utiliza otro elemento de la memoria se determina donde esta

- Busca los elementos necesarios y los coloca en el registro

- Ejecuta la instrucción

- vuelve al primer paso

Esta serie de pasos se conoce como el ciclo de búsqueda - decodificación - ejecución.



### Naturaleza del intérprete

El ciclo mencionado es fundamental para el funcionamiento de toda computadora. Pero pueden crearse programas que emulen su funcionalidad. De manera que las instrucciones dejen de ser realizadas por una máquina física y pasen a ser realizadas por otro programa. A este programa capas de realizar el ciclo se le llama interprete.

Él interprete ocupa un papel fundamental en la construcción de una computadora. Una vez establecido cual va a ser el lenguaje de máquina L, el diseñador puede elegir si construir un procesador en hardware que interprete directamente los programas en L o si prefiere escribir un interprete que interprete los programas en L, si elije un interprete la máquina en hardware debe ejecutar al intérprete. Él interprete descompone las instrucciones en pasos pequeños y las traduce al lenguaje de máquina L. De manera que la máquina en hardware puede construirse de forma menos compleja.

Con el tiempo se empezó a necesitar la implementación instrucciones más complejas, lo cual desencadeno en intérpretes más complejos y hardwares más potentes.

La empresa IBM introdujo el termino de Arquitectura para especificar la compatibilidad entre hardware y software en una línea de producción de computadoras. Con estos conceptos, la producción comenzó a generalizarse bajo la idea compatibilidad.

Con la idea de compatibilidad, y la posición del intérprete, comenzó a evitarse la complejidad en el hardware y destinar todo el peso al software, llevando toda la carga al intérprete, el cual podía realizar operación ya bastante complejas.

## Memoria primaria
La memoria es la parte de la computadora en la que se almacenan programas y datos. Sin una memoria en la cual los procesadores puedan leer y escribir información, no existiría las computadoras digitales de programa almacenado.

La unidad básica de memoria es el digito binario, llamado bit. Un bit puede contener un 0 o un 1, siendo la unidad más simple posible. Puede comprobarse fácilmente que es el método más confiable para codificar información digital.

Las memorias consisten en varias celdas o localidades, cada una de las cuales puede almacenar un elemento de información. Cada celda tiene un numero, su dirección con el cual los programas pueden referirse a ella. Si una memoria tiene n celdas, tendrá las direcciones 0 a n-1. Todas las celdas de una memoria contienen el mismo numero de bits. SI una celda consta de k bits, podra contener cualquiera de 2 elevado a k combinación de bit distintas.

## Conexión y finalidad.
Comprendiendo el CPU como al cerebro de la computadora y a la memoria como el almacén donde se guarda la información, que escribe y necesita el procesador, ahora debemos describir la comunicación entre ellos. El CPU cumple la finalidad de realizar una serie de instrucciones con un propósito, generalmente el resultado de este debe ser comunicado al usuario. El CPU se comunica con el usuario, ademas de con una serie de intérpretes que llevan la información de capa en capa para hacerla accesible, con dispositivos de entrada y salida. Son el medio mediante el cual la computadora brinda la información al medio físico. Así como un parlante puede, mediante las instrucciones del CPU, liberar un sonido al ambiente para que lo capte el usuario de la computadora. La forma más común es el medio visual que la computadora tiene para que el usuario puede ver e interactuar con ella, puede ser una pantalla, un impresión en papel, una proyección de luces, etc. Todos estos dispositivos con esta finalidad se llaman dispositivos de salida. En cambio los dispositivos de entrada son el canal por el cual el usuario brinda las instrucciones a la computadora. Mediante el dispositivo de salida el usuario puede visualizar el estado de la computadora y mediante algún medio de entrada como generalmente es un teclado, un marcador o un dispositivo de marcado, puede introducir señales que, nuevamente mediante una cadena de intérpretes, llega como instrucciones a la CPU que las realiza. Este ciclo de entrada y respuesta es la comunicación que un usuario tiene con la computadora y permite usarla como una herramienta. La conexión entre los dispositivos de entrada y salida, junto con la memoria y el procesador, se realiza mediante Bus de datos, Estos son cables especializados o circuitos planos flexibles capaces de trasmitir información entre los dispositivos, para así llegar a los intérpretes traduciéndola a cada hardware especifico.