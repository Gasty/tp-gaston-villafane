﻿## Bibliografía Usada para la primera parte del trabajo/ link:

- https://medium.com/@janpoloy/qu%C3%A9-es-y-para-que-sirve-git-3fd106e6e137 (Dado por la materia)
- https://rogerdudler.github.io/git-guide/index.es.html (Dado por la materia)
- https://www.genbeta.com/desarrollo/manejo-de-ramas-de-desarrollo-con-git (Dado por la materia)
- https://elc.github.io/posts/git-guide-with-visual-interface/es/ (Búsqueda propia)
- https://www.gitkraken.com/ (Búsqueda propia, para conseguir la interfaz visual)
- https://git-scm.com/book/es/v2/Ramificaciones-en-Git-Procedimientos-B%C3%A1sicos-para-Ramificar-y-Fusionar (Búsqueda propia, ramificación y marge)
- https://git-scm.com/book/es/v2/Appendix-B%3A-Comandos-de-Git-Ramificar-y-Fusionar (Búsqueda propia, fusionar)
- https://es.wikipedia.org/wiki/GitLab (Búsqueda propia, para una definición más comprensible)
- https://www.redeszone.net/2019/01/10/github-vs-gitlab-diferencias/ (Búsqueda propia, comenzó a interesarme y comencé a averiguar)