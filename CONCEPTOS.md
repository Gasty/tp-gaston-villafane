﻿# Trabajo practico 2

### Objetivos

- Familiarizarse con el lenguaje de programación de enriquecimiento de texto Markdown
- Aprender a utilizar git para control de versiones y programación colaborativa
- Aprender buenas prácticas de búsqueda y clasificación de información objetiva y plasmar el contenido procesado en un informe académico

### Alumno - Carrera
- Gastón Villafañe - Ingeniería Mecánica.

### Docentes responsables
- René Cejas Bolecek.
- Martin Vilugrón.


## Parte 1. Git y Markdown
### Respuesta 1 b.
- 1) Un software de control de versiones es un conjunto de programas o instrucciones destinado al seguimiento, control y manejo de un proyecto. Entendiendo como proyecto cualquier conjunto de archivos, software o datos en los que se trabaje. Un software de control de versiones ofrece una visión de todos los cambios de un proyecto con la posibilidad de volver a una version anterior a alguna modificación, esto permite tener un seguimiento de la evolución del proyecto a lo largo del tiempo. Es útil cuando hay varias personas involucradas, ya que lleva un registro de los aportes y alteraciones que hace cada participe del equipo.


- 2) Un repositorio es la carpeta o la zona en donde se guardaran los archivos del proyecto. El repositorio esta localmente en la computadora de cada miembro del trabajo y de aquí es donde se realizan las modificaciones que luego se subirán a la contraparte online del software de control de versiones de donde se actualizaran las carpetas de los repositorios de todos los que pertenezcan al proyecto. Se dice que el repositorio es publico cuando no hace falta ningún tipo de confirmación o membresía para acceder al proyecto. El repositorio de Bitcoin es un ejemplo de un repositorio publico. En cambio un repositorio privado es aquel que solo puede accederse al cumplir ciertas condiciones o, en la mayoría de los casos, solo al ser aceptado por el creador del proyecto.


- 3) Un árbol de Merkle es una estructura que organiza la información en ramificaciones de convergen a una raíz principal. Su huso el git es para la organización del proyecto mediante las branch, las cuales son versiones del proyecto que sigue una linea paralela a este. Esta rama se genera a partir de alguna version del proyecto elegido y permite modificarla y seguir una linea de evolución diferente de modo de no alterar la linea principal del proyecto.


- 4) Una copia local es aquella que se realiza en una unidad de almacenamiento que está localmente conectada al equipo. En cambio la copia remota o backup online es un servicio a través del cual se puede realizar una copia de sus datos a un dispositivo remoto a través de Internet.


- 5) Un commit es el estado de un proyecto en un determinado momento de la historia del mismo. Es decir, es una de las versiones del proyecto, que tuvo durante un tiempo antes de su modificación. Muchos software de control de versiones hacen un commit para cada modificación por mas pequeña que sea, pero esta preferencia puede ajustarse, para que se realice un commit solo cuando se establezca una modificación importante, trabajando desde el repositorio local y luego subiéndolo cuando quiera aceptar esta nueva version.


- 6) Como el software de control de versiones se encarga de dividir el proyecto en commit a lo largo su evolución, solo haría falta borrar los commit por delante de la version antigua que desea recuperarse. De no ser posible borrar los commit (como es en algunos software), simplemente descargaría el proyecto antiguo y lo subiría como un nuevo commit. Otra forma es generar un Branch, en el proyecto antiguo para trabajar de forma paralela, solo si no se quiere perder el avance hasta ahora por mas erróneo que este el trabajo.


- 7) Las brach, antes mencionadas, son lineas paralelas a la linea principal del proyecto, estas contienen una version antigua del proyecto que fue modificada a la par del mismo. Suele ser usado para corregir errores, probar versiones o generar ideas sobre la rama principal. De manera que una ves finalizada las pruebas en una rama, esta podría re agregarse a la linea principal de proyecto.


### Respuesta 2 b.
- 1) Markdown es un lenguaje de marcado que facilita el proceso de darle formato a un texto mediante símbolos entre las palabras. Esta diseñado para ser ligero y ágil reemplazando los pesados y complejos editores de texto. 


- 2) Es muy usado para subir texto a Internet ya que es rápido y fácil de usar. Pero también es usado por gente que necesita hacer textos cortos de formato simple.